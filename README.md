# BioVM - a virtual machine for Bioinformatics

BioVM is a virtual machine based on Ubuntu Linux (MATE) with
pre-configured software for Bioinformatics.

* Current stable release: [18.04][release]
* License: [GPLv3][license]
* [Website][website]
* [Documentation][documentation]
* [Source code][gitlab]

### Basic requirements

1. [VirtualBox][virtualbox] installed on your computer.
2. RAM - 4GB or more is recommended.  
   Note: The programs you intend to use for your analysis might require more
   memory.

## Getting BioVM

You can either purchase a ready to use virtual appliance or build and 
launch the VM manually from source code.

### Option 1: Get the appliance

Purchase a ready to use virtual machine (OVA format).

<a href="https://gum.co/ZMQNp">Purchase BioVM</a>

You will receive a link to download the appliance. Once the file is 
downloaded, open VirtualBox and then use the **File** -> **Import Appliance**
option and select the downloaded file (`biovm-{version}-amd64.ova`).

If you would like to change the **number of CPU's** or the **amount of
RAM** available for the appliance, you can do so at this stage.

Once import is complete, select **BioVM** in the VirtualBox main
window and the click on the  **Start** button to launch.

### Option 2: Build and launch VM

#### Install requirements

1. [Vagrant][vagrant] - used for building the virtual machine using VirtualBox as a provider.
2. [Ansible][ansible] - used for provisoning the various roles.
3. [Git][git] - for cloning the repository.

On Ubuntu, these packages can be installed directly from the repositories:

    sudo apt install vagrant ansible git
    
*These steps have been tested on Ubuntu 16.04 & 18.04*.

#### Clone repository and launch VM

You can use Git to clone and check out the master branch directly:

    git clone -b master https://gitlab.com/biovm/biovm
    
The *master* branch corresponds to the latest released version. The 
*develop* branch contains the most recent version (unstable).

Launch the virtual machine using vagrant::

    cd biovm
    vagrant up

In both cases, the virtual machine will boot directly to the desktop.

## Documentation & Support

Online documentation is available at: https://biovm.gitlab.io.

For any questions that are directly related to BioVM and are
not answered in the documentation, please post them on
[Biostars][biostars] with the tag 
"biovm". If you notice any bugs, please 
report them on [Gitlab](https://gitlab.com/biovm/biovm/issues).

If you have purchased the appliance, you can send an email to
support@vimal.io with your query.

## Customisation

This virtual machine is built using the `ubuntu/xenial64` base image. 
Tasks are defined as ansible roles. For example, the **biovm-core** role
installs the core Bioinformatics software included in BioVM. Additional
roles can be defined and then included in **setup.yml**.


<!-- Links -->

[ansible]: https://www.ansible.com/
[biostars]: https://biostars.org
[gumroad]: https://gumroad.com/l/ZMQNp
[git]: https://git-scm.com
[gitlab]: https://gitlab.com/biovm/biovm
[license]: https://www.gnu.org/licenses/gpl.html
[release]: https://gitlab.com/biovm/biovm/releases/tag/18.04
[virtualbox]: https://www.virtualbox.org/
[vagrant]: https://www.vagrantup.com/
[website]: https://vimal.io/biovm
[documentation]: https://biovm.gitlab.io


